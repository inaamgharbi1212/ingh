// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDZcxRkY9HK0k-dRDuJ-fN8x3flwk4i8E4",
    authDomain: "vermegproject-c0655.firebaseapp.com",
    databaseURL: "https://vermegproject-c0655.firebaseio.com",
    projectId: "vermegproject-c0655",
    storageBucket: "",
    messagingSenderId: "1099326806990"
  }
};


/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
