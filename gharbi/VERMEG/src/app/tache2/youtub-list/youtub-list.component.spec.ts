import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YoutubListComponent } from './youtub-list.component';

describe('YoutubListComponent', () => {
  let component: YoutubListComponent;
  let fixture: ComponentFixture<YoutubListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YoutubListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutubListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
