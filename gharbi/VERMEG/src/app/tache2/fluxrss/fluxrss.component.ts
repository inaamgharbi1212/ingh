import { Component, OnInit } from '@angular/core';

  import { FeedService } from 'src/app/tache2/feed-service.service';

@Component({
  selector: 'app-fluxrss',
  templateUrl: './fluxrss.component.html',
  styleUrls: ['./fluxrss.component.css']
})
export class FluxrssComponent implements OnInit {
  //private feedUrl: string = 'https://www.youtube.com/feeds/videos.xml?channel_id=UCm5wThREh298TkK8hCT9HuA';
  private feedUrl: string = 'https://www.youtube.com/feeds/videos.xml?channel_id=UCHnyfMqiRRG1u-2MsSQLbXA ';
  //private feedUrl: string = ' https://www.youtube.com/feeds/videos.xml?channel_id=UCoookXUzPciGrEZEXmh4Jjg';
  //private feedUrl: string = 'https://www.youtube.com/feeds/videos.xml?playlist_id=PLFgquLnL59ak5FwmTB7DRJqX3M2B1D7xI';
  private feeds:any

  constructor (
    private feedService: FeedService 
  ) {}

  ngOnInit() {
    this.refreshFeed();
    
  }

  private refreshFeed() {
    this.feedService.getFeedContent(this.feedUrl)
        .subscribe(
            feed => this.feeds = feed.items ,
            error => console.log(error)) ;
  }

}