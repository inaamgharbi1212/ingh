import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FluxrssComponent } from './fluxrss.component';

describe('FluxrssComponent', () => {
  let component: FluxrssComponent;
  let fixture: ComponentFixture<FluxrssComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FluxrssComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FluxrssComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
