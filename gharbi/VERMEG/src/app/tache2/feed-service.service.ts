import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {Observable} from 'rxjs';
import { map, filter, catchError, mergeMap } from 'rxjs/operators';
import { Feed } from '././Model/feed'
import 'rxjs/Rx';
import 'rxjs/add/operator/toPromise';
import { SettingComponent } from '../mestaches/setting/setting.component';

import 'rxjs/add/operator/map'
import { AngularFireList } from 'angularfire2/database';
@Injectable()
export class FeedService {
link: AngularFireList<any>;
  private rssToJsonServiceBaseUrl: string = 'https://rss2json.com/api.json?rss_url=';

  constructor(
    private http: Http
  ) { }

  getFeedContent(url: string): Observable<Feed> {
    return this.http.get(this.rssToJsonServiceBaseUrl + url)
            .map(this.extractFeeds)
            .catch(this.handleError);
  }

  private extractFeeds(res: Response): Feed {
    let feed = res.json();
    return feed || { };
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }

  }
