import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
@Component({
  selector: 'app-feed-card',
  templateUrl: './feed-card.component.html',
  styleUrls: ['./feed-card.component.css']
})
export class FeedCardComponent implements OnInit {
 
  x: string;

  @Input() feed: any;
  public YT: any;
  public player: any;
  public reframed: Boolean = false;

  constructor(private sanitizer: DomSanitizer) { 
    
  }
  transform(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
  openLinkInBrowser() {
    window.open(this.feed.link);
    //return this.x = this.feed.link ,
   // console.log("x")
  }

  openUrl(){
    return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + this.feed.link.substr(this.feed.link.length-11))
  }
  ngOnInit(){
    console.log(this.openUrl())
  /// this.init();
    //window['onYouTubeIframeAPIReady'] = (e) => {
     // this.YT = window['YT'];
     // this.reframed = false;
     // this.player = new window['YT'].Player('player', {
      //  videoId:  this.feed,
      //});
   // }
  
  
//  init() {
    //var tag = document.createElement('script');
    //tag.src = 'https://www.youtube.com/iframe_api';
    //var firstScriptTag = document.getElementsByTagName('script')[0];
    //firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  //}


  }}

