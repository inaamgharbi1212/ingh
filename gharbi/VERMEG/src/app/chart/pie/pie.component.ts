import { Component, OnInit } from '@angular/core';
import { Mission } from '../../Models/mission';
import { CustomUser } from '../../dash/dashadmin/CustomUser';
import { AngularFireDatabase } from 'angularfire2/database';
import { Message } from 'primeng/components/common/message';
import { User } from '../../Models/user';

@Component({
  selector: 'app-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.css']
})
export class PieComponent implements OnInit {

  data: any;

  missionList: Mission[] = [];
  currentUser = {} as User;
  userList: Mission[] = [];
  showLoading: boolean = true;
  display: boolean = false;
  userToShow: Mission;
  msgs: Message[] = [];
  users : CustomUser[] = []

  constructor(private afd: AngularFireDatabase) { 
      
      
/*     this.data = {
        labels: ['A','B','C','D'],
        datasets: [
            {
                label : "# of users",
                data: [1, 2,3,4],
                backgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56",
                    '#666666'
                ],
                hoverBackgroundColor: [
                    "#FF6384",
                    "#36A2EB",
                    "#FFCE56",
                    '#666666'
                ]
            }]    
        }; */

  }
  createChart(labels,datas) {
    let chart = {
        "labels" : labels,
        "datasets" : [
            {
                "label" : "# of users",
                "data" : datas,
                backgroundColor: [
                    this.getRandomColorHex(),
                    this.getRandomColorHex(),
                    this.getRandomColorHex(),
                    this.getRandomColorHex(),
                    this.getRandomColorHex()
                  ],
                hoverBackgroundColor:[
                  
                    this.getRandomColorHex(),
                    this.getRandomColorHex(),
                    this.getRandomColorHex(),
                    this.getRandomColorHex(),
                    this.getRandomColorHex()
                      
                ]
            }
        ]
    }
    return chart
  }
/*   fetchData() {

    this.showLoading = true;
    this.afd.list("/users/")
      .snapshotChanges().subscribe(data => {
        this.userList = [];
        data.forEach(element => {
          var y = element.payload.toJSON();
          y["$key"] = element.key;
          this.userList.push(y as Mission);
        });
        this.showLoading = false;
        console.log(this.userList);

        this.userList.forEach(elem => {
            this.users.forEach(user => {
                console.log(elem)
            })
        })
      });
  } */


    fetchData() {
    this.showLoading = true;
    this.afd.list("/missions/")
      .snapshotChanges().subscribe(data => {
        this.missionList = [];
        data.forEach(element => {
          var y = element.payload.toJSON();
          y["$key"] = element.key;
          this.missionList.push(y as Mission);
        });
        this.showLoading = false;
        let ourUser : CustomUser = {
            firstName : this.missionList[0].user.firstname,
            lastName : this.missionList[0].user.lastname,
            email : this.missionList[0].user.email,
            nbOfMissions : 1 
          
        }
        console.log(ourUser)
        this.users.push(ourUser)
        this.missionList.forEach((mission,index) => {

             if(index>0){
                let isExist : boolean = false
                let indexIsExist : number
                 this.users.forEach((customUser,index2) => {
                   
                     if(mission.user.email == customUser.email){
                         isExist=true
                         indexIsExist=index2

                    }  
                }) 
                if(!isExist){
                    let tempUser : CustomUser = {
                        firstName : mission.user.firstname,
                        lastName : mission.user.lastname,
                        email : mission.user.email,
                        nbOfMissions : 1
                     }
                    this.users.push(tempUser)
                    console.log(this.users)
             
                }
                else{
                    this.users[indexIsExist].nbOfMissions++
                  
                }
            } 
        })

        let labels : string [] = []
        let datas : number [] = []
        this.users.forEach(item => {
            labels.push(item.firstName)
            datas.push(item.nbOfMissions)
            console.log(item.firstName)
            console.log(item.nbOfMissions)
        })
        this.data=this.createChart(labels,datas)
        console.log(this.data)
      });
      

  }
 getRandomColorHex() {
    var hex = "0123456789ABCDEF",
        color = "#";
    for (var i = 1; i <= 6; i++) {
      color += hex[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  ngOnInit() {
    this.fetchData()
    console.log(this.users)
  }
}

