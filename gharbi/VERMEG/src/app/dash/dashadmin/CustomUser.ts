export class CustomUser {
    firstName : string
    lastName : string
    email : string
    nbOfMissions : number
}