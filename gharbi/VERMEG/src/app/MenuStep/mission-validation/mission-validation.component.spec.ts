import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionValidationComponent } from './mission-validation.component';

describe('MissionValidationComponent', () => {
  let component: MissionValidationComponent;
  let fixture: ComponentFixture<MissionValidationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionValidationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionValidationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
