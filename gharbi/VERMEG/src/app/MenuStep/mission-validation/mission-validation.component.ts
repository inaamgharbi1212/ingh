import { Router } from '@angular/router';
import { User } from './../../Models/user';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/primeng';
import { MissionService } from './../../services/mission.service';
import { Mission } from './../../Models/mission';
import { Component, OnInit, Input } from '@angular/core';
import { GrowlModule } from 'primeng/growl';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'app-mission-validation',
  templateUrl: './mission-validation.component.html',
  styleUrls: ['./mission-validation.component.css']
})
export class MissionValidationComponent implements OnInit {

  @Input('mission') mission: Mission;
  msgs: Message[] = [];


  constructor(private missionService: MissionService, 
    private messageService: MessageService,
    private router:Router
  ) { }

  ngOnInit() {
    console.log(this.mission);
    this.mission.missionTotalPrice = (Number(this.mission.missionFlight.price) + Number(this.mission.missionHotel.total_price.amount)).toString();
  }

  onValidate() {

    let currentUser = JSON.parse(localStorage.getItem("currentUser")) as User;
    this.mission.user = currentUser;
    this.mission.missionStatus = "pending";
    console.log(this.mission);

    this.missionService.insertMission(this.mission).then(data=>{
      this.messageService.add({ severity: 'success', summary: 'Success ! ', detail: "Mission has been added successfully" });
      
      setTimeout(()=>{

        this.router.navigate["/history"] ;

      },1000);
    
    });

  }

}
