import { Mission } from './../../Models/mission';
import { Component, OnInit } from '@angular/core';
import { Message } from 'primeng/components/common/message';
import { FormGroup } from '@angular/forms';
import { SelectItem } from 'primeng/components/common/selectitem';
import { FormBuilder } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { MenuItem } from 'primeng/primeng';
import { ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'app-descr',
    templateUrl: './descr.component.html',
    styles: [`
  .ui-steps .ui-steps-item {
      width: 32.2%;
      margin-top: 5%;

    
   
  }
  .container{
      margin-left: 2%
  }
  
  .ui-steps.steps-custom {
      margin-bottom: 30px;
      
  }
   
  .ui-steps.steps-custom .ui-steps-item .ui-menuitem-link {
      height: 10px;
      padding: 0 1em;
  }
   
  .ui-steps.steps-custom .ui-steps-item .ui-steps-number {
      background-color: #0081c2;
      color: #FFFFFF;
      display: inline-block;
      width: 36px;
      border-radius: 100%;
      margin-top: -14px;
      margin-bottom: 10px;
  }
  
  .ui-steps.steps-custom .ui-steps-item .ui-steps-title {
      color: #555555;
  }
 
`],
    encapsulation: ViewEncapsulation.None
})

export class DescrComponent implements OnInit {

    items: MenuItem[];

    activeIndex: number = 0;

    msgs: Message[] = [];


    userform: FormGroup;

    submitted: boolean;

    genders: SelectItem[];

    description: string;

    missionDetails = {} as Mission;

    constructor(private fb: FormBuilder) { }

    ngOnInit() {

        this.missionDetails.paperType = 'no';

        this.items = [{
            label: '  Mission Description',
            command: (event: any) => {
                this.msgs.length = 0;
                this.msgs.push({ severity: 'info', summary: 'Mission Description', detail: event.item.label });

            }
        },
        {
            label: 'Booking',
            command: (event: any) => {
                this.msgs.length = 0;
                this.msgs.push({ severity: 'info', summary: 'Booking', detail: event.item.label });


            }

        },
        {
            label: 'Validation',
            command: (event: any) => {
                this.msgs.length = 0;
                this.msgs.push({ severity: 'info', summary: 'validation', detail: event.item.label });
            }
        },

        ];
    }


    nextStep($event) {
        this.activeIndex = $event;
    }

}
