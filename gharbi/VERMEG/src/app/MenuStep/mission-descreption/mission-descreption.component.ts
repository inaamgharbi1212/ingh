import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { Mission } from './../../Models/mission';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PanelModule } from 'primeng/panel';
import { SelectItem } from 'primeng/api';
import { FileUploadModule } from 'primeng/fileupload';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GrowlModule } from 'primeng/growl';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';


@Component({
  selector: 'app-mission-descreption',
  templateUrl: './mission-descreption.component.html',
  styleUrls: ['./mission-descreption.component.css']
})
export class MissionDescreptionComponent implements OnInit {
  @Input('mission') mission: Mission;
  @Input('activeIndex') activeIndex: number;
  @Output() nextStep = new EventEmitter<number>();

  msgs: Message[] = [];

  paperType: SelectItem[];
  

  missionDescreptionForm = new FormGroup({

    missionObject: new FormControl('', [Validators.required]),
    missionDestionation: new FormControl('', [Validators.required]),
    missionCity: new FormControl('', [Validators.required]),
    userWebsite: new FormControl('', [Validators.required]),
    missionStartDate: new FormControl('', [Validators.required]),
    missionEndDate: new FormControl('', [Validators.required]),
  });

  constructor(private messageService: MessageService) { }

  ngOnInit() {

      this.paperType = [
      { label: 'No', value: "no", icon: "fas fa-times-circle", },
      { label: 'VISA', value: "visa", icon: "fab fa-fw fa-cc-visa" },
      { label: 'Passport', value: "passport", icon: "fas fa-fw fa-passport" },
    ];

  }

  goToNextStep() {
    this.nextStep.emit(this.activeIndex++);
  }


  checkDateValidity() : boolean{
    let todayDateTimestamp = new Date().valueOf();
    let startDateTimestamp = new Date(this.mission.startDate).valueOf() ; 
    let endDateTimestamp = new Date(this.mission.endDate).valueOf();
    if(startDateTimestamp>=endDateTimestamp || startDateTimestamp<todayDateTimestamp) return false ;
    else return true ;
  }

  submitMission() {
    
    if (!this.missionDescreptionForm.valid) {
      this.messageService.add({ severity: 'error', summary: 'Invalid Form', detail: 'Verify all the fields ' });
    } else if(this.checkDateValidity()==false){
      this.messageService.add({ severity: 'error', summary: 'Invalid Form', detail: 'Verify Start Date and End Date ' });
    }else{
      this.goToNextStep();
    }

  }

}
