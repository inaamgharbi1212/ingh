import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionDescreptionComponent } from './mission-descreption.component';

describe('MissionDescreptionComponent', () => {
  let component: MissionDescreptionComponent;
  let fixture: ComponentFixture<MissionDescreptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionDescreptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionDescreptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
