import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MissionFlightComponent } from './mission-flight.component';

describe('MissionFlightComponent', () => {
  let component: MissionFlightComponent;
  let fixture: ComponentFixture<MissionFlightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MissionFlightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MissionFlightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
