import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/components/common/api';
import { element } from 'protractor';
import { Http, Response, ResponseOptions } from '@angular/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Mission } from './../../Models/mission';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { GrowlModule } from 'primeng/growl';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';


@Component({
  selector: 'app-mission-flight',
  templateUrl: './mission-flight.component.html',
  styleUrls: ['./mission-flight.component.css']
})
export class MissionFlightComponent implements OnInit {

  @Input('mission') mission: Mission;
  @Input('activeIndex') activeIndex: number;
  @Output() nextStep = new EventEmitter<number>();
  missionBooking = new FormGroup({

    flightDeparture: new FormControl('', Validators.required),
    flightArrival: new FormControl('', Validators.required),
    flightDate: new FormControl('', Validators.required),

  });
  volDisp: any[] = [];

  hoteldispo: any[] = [];

  isSelectHotel: boolean = false;

  isSelectFlight: boolean = false;

  textDeparture: string;

  resultsDeparture: string[];


  textArrival: string;


  resultsArrival: string[];


  airportsAutoCompleteAPI = "https://api.sandbox.amadeus.com/v1.2/airports/autocomplete?apikey=bGr4xlAR788s0XByJDeQkAjeYdcgQNQy";

  flightsAPI = "https://api.sandbox.amadeus.com/v1.2/flights/inspiration-search?apikey=bGr4xlAR788s0XByJDeQkAjeYdcgQNQy";

  hotelsAPI = "https://api.sandbox.amadeus.com/v1.2/hotels/search-airport?apikey=bGr4xlAR788s0XByJDeQkAjeYdcgQNQy&location=BOS&check_in=2019-03-02&check_out=2019-03-04";


  flightStartDate : any ; 
  constructor(private http: Http, private messageService: MessageService) { }

  ngOnInit() {
  }

  
  searchDeparture($event) {
    console.log("searching");

    this.http.get(this.airportsAutoCompleteAPI + "&term=" + this.textDeparture).subscribe((response: Response) => {
      console.log(response.json());
      let airportsArray = response.json() as string[];
      let tmpArray: string[] = [];
      airportsArray.forEach((element: any) => {
        tmpArray.push(element.value + " - " + element.label);
      });
      this.resultsDeparture = tmpArray;
    });
  }


  checkDateValidity(): boolean {
    let todayDateTimestamp = new Date().valueOf();
    let startDateTimestamp = new Date(this.mission.startDate).valueOf();
    let flightDateTimestamp = new Date(this.flightStartDate).valueOf();
    if (startDateTimestamp > flightDateTimestamp || flightDateTimestamp<todayDateTimestamp) return false;
    else return true;
  }

  searchArrival($event) {


    console.log("searching");
    this.http.get(this.airportsAutoCompleteAPI + "&term=" + this.textArrival).subscribe((response: Response) => {
      let airportsArray = response.json() as string[];
      let tmpArray: string[] = [];
      airportsArray.forEach((element: any) => {
        tmpArray.push(element.value + " - " + element.label);
      });
      this.resultsArrival = tmpArray;
    });


  }


  searchFligh($event) {


    if (this.checkDateValidity() == false) {
      this.messageService.add({ severity: 'error', summary: 'Invalid Form', detail: 'Verify Flight Date !' });
    } else {

      let arrival = this.textArrival.substr(0, 3).trim();
      let dept = this.textDeparture.substr(0, 3).trim();

      this.http.get(this.flightsAPI + "&origin=" + dept + "&destination=" + arrival).toPromise().then(
        (response: Response) => {
          console.log(response.json());

          this.volDisp = response.json().results;

          this.messageService.add({ severity: 'info', summary: 'Search done', detail: this.volDisp.length + ' flights available !' });


        }
      ).catch(err => {
        console.log("erreyr");
        this.volDisp = [];
        this.messageService.add({ severity: 'info', summary: 'Error', detail: 'No flights available !' });

        console.log(err);
      });


      this.http.get(this.hotelsAPI + "&location=" + arrival).toPromise().then((response: Response) => {

        console.log("hotels");
        console.log(response.json().results);
        this.hoteldispo = response.json().results;

        this.messageService.add({ severity: 'info', summary: 'Search done', detail: this.hoteldispo.length + ' hotels available !' });


      }).catch(err => {

        this.messageService.add({ severity: 'info', summary: 'Error', detail: 'No hotels available !' });

      });
    }

  }


  selectFlight($event) {
    console.log($event);
   
    this.messageService.add({ severity: 'info', summary: 'Selected', detail: 'A Flight has been selected !' });
    this.mission.missionFlight = $event.data;
    this.isSelectFlight = true;
  }

  selectHotel($event) {
    console.log($event);
    this.messageService.add({ severity: 'info', summary: 'Selected', detail: 'A Hotel has been selected !' });
    this.mission.missionHotel = $event.data;
    this.isSelectHotel = true;
  }


  submitFlightHotelMission() {
    console.log(this.mission);
    console.log(this.mission.missionFlight);
    if (this.missionBooking.invalid || this.isSelectFlight == false || this.isSelectHotel == false) {
      console.log("invalid");
      this.messageService.add({ severity: 'error', summary: 'Invalid Form', detail: 'The form is invalid !' });


    } else {

      this.goToNextStep();

    }


  }

  goToNextStep() {
    this.nextStep.emit(this.activeIndex++);
  }




}
