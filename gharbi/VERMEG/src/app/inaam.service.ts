import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
@Injectable({
  providedIn: 'root'
})
export class InaamService {
  link: AngularFireList<any>;
  showDeletedMessage: boolean;
  constructor(private afd: AngularFireDatabase,
  ) { }
  deletelink($key : string) {
    this.afd.list("/link/" + $key).remove()

  }
}
