import { Component, OnInit } from '@angular/core';
import { Link } from './link';
import { AngularFireDatabase } from 'angularfire2/database';


@Component({
  selector: 'app-slide1',
  templateUrl: './slide1.component.html',
  styleUrls: ['./slide1.component.css']
})
export class Slide1Component implements OnInit {
  links: Link[] = [];
  constructor(private afd: AngularFireDatabase
 ) { }

  ngOnInit() {
    this.getAllLinks();
  }


  getAllLinks(){

  
    this.afd.list("/Link/")
      .snapshotChanges().subscribe(data => {
        this.links = [];
        data.forEach(element => {
          var y = element.payload.toJSON();
          y["$key"] = element.key;
          this.links.push(y as Link);
        });
      
        console.log(this.links);

      });
  }
}