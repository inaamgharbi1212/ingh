import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuslideComponent } from './menuslide.component';

describe('MenuslideComponent', () => {
  let component: MenuslideComponent;
  let fixture: ComponentFixture<MenuslideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuslideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuslideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
