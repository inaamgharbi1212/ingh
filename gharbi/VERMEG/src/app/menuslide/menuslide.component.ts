import { Component, OnInit } from '@angular/core';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { GrowlModule } from 'primeng/growl';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { User } from '../Models/user';
import { AngularFireDatabase } from 'angularfire2/database';
import { Mission } from '../Models/mission';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-menuslide',
  templateUrl: './menuslide.component.html',
 styles: [`
 .tab{
  margin-top: 25px;
  width : 140%

}
.tab .nav-tabs{
  border:none;
  border-bottom: 1px solid #03649c;
  width : 100%
 
}
.nav-tabs li a{
  padding: 15px 40px;
  border:1px solid #ededed;
  border-top: 2px solid #ededed;
  border-right: 0px none;
  background:hsl(202, 71%, 60%);
  color: rgb(250, 250, 250);
  border-radius: 0px;
  margin-right: 0px;
  font-weight: bold;
  transition: all 0.3s ease-in 0s;

}
.nav-tabs li a:hover{
  border-bottom-color: #ededed;
  border-right: 0px none;
  color: #0e0d0d;
  color: #0e0d0d;
}
.nav-tabs li a i{
  display: inline-block;
  text-align: center;
  margin-right:10px;
}
.nav-tabs li:last-child{
  border-right:1px solid #ededed;
}
.nav-tabs li.active a,
.nav-tabs li.active a:focus,
.nav-tabs li.active a:hover{
  border-top: 3px solid #00b0ad;
  border-right: 1px solid #d3d3d3;
  margin-top: -15px;
  color: gb(250, 250, 250);
  padding: 22px 40px;
}
.tab .tab-content{
  padding: 20px;
  line-height: 22px;
  box-shadow:0px 1px 0px #808080;

}
.tab .tab-content h3{
  margin-top: 0;
}
@media only screen and (max-width:500%){
  .nav-tabs li{
      width:100%;
      margin-bottom: 10px;
  }
  .nav-tabs li a{
      padding: 15px;
  }
  .nav-tabs li.active a,
  .nav-tabs li.active a:focus,
  .nav-tabs li.active a:hover{
      padding: 15px;
      margin-top: 0;
  }
}



 
`],
})
export class MenuslideComponent implements OnInit {
  currentUser = {} as User ;
  missionList: Mission[] = [];
  show: boolean= false;
  data: any;
  msgs: Message[] = [];
  index: number = 0;
  constructor(private messageService: MessageService,
    private afd: AngularFireDatabase,) {
    this.data = {
      labels: ['A','B','C'],
      datasets: [
          {
              data: [300, 50, 100],
              backgroundColor: [
                  "#FF6384",
                  "#36A2EB",
                  "#FFCE56"
              ],
              hoverBackgroundColor: [
                  "#FF6384",
                  "#36A2EB",
                  "#FFCE56"
              ]
          }]    
      };
   }

  ngOnInit() {
    this.fetchData();

  }
  
  openNext() {
    this.index = (this.index === 2) ? 0 : this.index + 1;
    this.messageService.add({ severity: 'error', summary: 'Login error', detail: 'Form is Invalid ! ' });
}


  openPrev() {
    this.index = (this.index === 0) ? 2 : this.index - 1;
}
showp() {
  this.show = !this.show
}
fetchData() {

  this.currentUser = JSON.parse(localStorage.getItem("currentUser")) as User;


  this.afd.list("/missions/", ref => ref.orderByChild("user/uid").equalTo(this.currentUser.uid))
    .snapshotChanges().subscribe(data => {
      this.missionList = [];
      data.forEach(element => {
        var y = element.payload.toJSON();
        y["$key"] = element.key;
        this.missionList.push(y as Mission);
      });
   
      console.log(this.missionList);

    });
}


}
