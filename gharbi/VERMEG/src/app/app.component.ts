import { User } from './Models/user';
import { UserService } from './services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'app';

  userIsLoggedIn: boolean = false;



  constructor(public userService: UserService, private router: Router) {
    

  }

  ngOnInit() {


    

    
    this.userService.isLoggedIn().subscribe(userData => {
      let currentUser = JSON.parse(localStorage.getItem("currentUser")) as User;

      if (userData==null || currentUser==null) {
        console.log("not logged");

        this.userIsLoggedIn = false;

      } else {
        console.log("logged in");
        this.userIsLoggedIn = true;
        if(currentUser.userType=="collaborator"){
          this.router.navigate(['/history']);
        }else if (currentUser.userType=="admin"){
          this.router.navigate(['/usersAdmin']);
        }else{
          this.router.navigate(['/validateMissions']);
        }
        
      }
    });
  }

}
