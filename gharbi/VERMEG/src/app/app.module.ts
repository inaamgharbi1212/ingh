// ***les modules de primeNG***////
import { AccordionModule } from 'primeng/accordion';    
import { MenuItem } from 'primeng/api';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { StepsModule } from 'primeng/steps';
import { ButtonModule } from 'primeng/button';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { PasswordModule } from 'primeng/password';
import { DataTableModule, SharedModule, DataTable, PanelModule, PaginatorModule, DialogModule } from 'primeng/primeng';
import { TableModule } from 'primeng/table';
import { CalendarModule } from 'primeng/calendar';
import { DataViewModule } from 'primeng/dataview';
import { CheckboxModule } from 'primeng/checkbox';
import { GrowlModule } from 'primeng/primeng';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { MenubarModule } from 'primeng/menubar';
import { ProgressBarModule } from 'primeng/progressbar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { SelectButtonModule } from 'primeng/selectbutton';
import { FileUploadModule } from 'primeng/fileupload';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import {DropdownModule} from 'primeng/dropdown';
import { ReactiveFormsModule } from '@angular/forms';
import {ChartModule} from 'primeng/chart';
import { HttpClientModule } from '@angular/common/http';
import {ToolbarModule} from 'primeng/toolbar';


import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';

//***mes composants****
import { AppComponent } from './app.component';
import { SigninComponent } from './components/signin/signin.component';
import { MissionHistoryComponent } from './components/mission-history/mission-history.component';
import { MissionFlightComponent } from './MenuStep/mission-flight/mission-flight.component';
import { MissionValidationComponent } from './MenuStep/mission-validation/mission-validation.component';
import { ValidateMissionsComponent } from './components/validate-missions/validate-missions.component';
import { UsersAdminComponent } from './components/users-admin/users-admin.component';
import { AddUserAdminComponent } from './components/add-user-admin/add-user-admin.component';
import { ProfileAdminComponent } from './profiles/profile-admin/profile-admin.component';
import { DashadminComponent } from './dash/dashadmin/dashadmin.component';

import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { DescrComponent } from './MenuStep/descr/descr.component';
import { MissionDescreptionComponent } from './MenuStep/mission-descreption/mission-descreption.component';
//**mes modules**
import { HttpModule } from '@angular/http';
import {TabViewModule} from 'primeng/tabview';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';
//**configuration Backend **firebase**
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireObject } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FirebaseDatabase } from '@firebase/database-types';
import { AngularFireDatabase } from "angularfire2/database-deprecated";





//***environement ***
import { environment } from '../environments/environment';
//***mes service ****
import { UserService } from './services/user.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { MenuslideComponent } from './menuslide/menuslide.component';
import { Slide1Component } from './slide1/slide1.component';
import {ListboxModule} from 'primeng/listbox';
import {MultiSelectModule} from 'primeng/multiselect';

import { SettingComponent } from './mestaches/setting/setting.component';


import { ChartsComponent } from './components/tache/charts/charts.component';
import {LightboxModule} from 'primeng/lightbox';
import { DashcollaComponent } from './dashcolla/dashcolla.component';
import { InaamtestComponent } from './inaamtest/inaamtest.component';
import { FluxrssComponent } from './tache2/fluxrss/fluxrss.component';
import { FeedCardComponent } from './tache2/feed-card/feed-card.component';


import { StripHtmlTagsPipe } from './tache2/strip-html-tags.pipe';
import { FeeddComponent } from './components/tache3/feedd/feedd.component';
import { FluxComponent } from './components/tache3/flux/flux.component';
import { FeedService } from './tache2/feed-service.service';
import { FeedssService } from './components/tache3/feedss.service';
import { YoutubListComponent } from './tache2/youtub-list/youtub-list.component';

import {ScrollPanelModule} from 'primeng/scrollpanel';
import { BarComponent } from './chart/bar/bar.component';
import { LineComponent } from './chart/line/line.component';
import { PieComponent } from './chart/pie/pie.component';
import { TestapiComponent } from './testapi/testapi.component';
import { TestapiService } from './testapi/testapi.service';
import { MergeRequestAPIService } from './services/merge-request-api.service';
import { MergeRequestAPIComponent } from './mergeRequestAPI/merge-request-api/merge-request-api.component';
import { ListBranchesComponent } from './components/list-branches/list-branches.component';
import { ListOfCommitComponent } from './components/list-of-commit/list-of-commit.component';




// { path: '', component: HomeComponent },
const appRoutes: Routes = [


  { path: '', component: SigninComponent, outlet: 'loginOutlet' },
  { path: 'home', component: HomeComponent },
  { path: 'history', component: MissionHistoryComponent },
  { path: 'descr', component: DescrComponent },
  { path: 'validateMissions', component: ValidateMissionsComponent },
  { path: 'register', component: RegisterComponent, outlet: 'loginOutlet' },
  { path: 'usersAdmin', component: UsersAdminComponent },
  { path: 'addUserAdmin', component: AddUserAdminComponent },
  { path: 'profile-admin', component:  ProfileAdminComponent },
  { path: 'dashcol', component:  DashadminComponent },
  { path: 'dashval', component:  DashadminComponent },
  { path: 'menuslide', component:  MenuslideComponent },
  { path: 'slide1', component:  Slide1Component },
  { path: 'setting', component:  SettingComponent },
  { path: 'x', component:   DashcollaComponent },
  { path: 'rss', component:   FluxrssComponent },
  { path: 'reader', component:   FluxComponent },
  { path: 'inaam', component:   BarComponent },
  { path: 'line', component:   LineComponent },
  { path: 'pie', component:   PieComponent },
  { path: 'branche', component:   ListBranchesComponent },




 
  
  
  
]


@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    RegisterComponent,
    HomeComponent,
    DescrComponent,
    StripHtmlTagsPipe,
    MissionHistoryComponent,
    MissionDescreptionComponent,
    MissionFlightComponent,
    MissionValidationComponent,
    ValidateMissionsComponent,
    UsersAdminComponent,
    AddUserAdminComponent,
    ProfileAdminComponent,
    DashadminComponent,
     MenuslideComponent,
    Slide1Component,
    SettingComponent,
   
    ChartsComponent,
    DashcollaComponent,
    InaamtestComponent,
    FluxrssComponent,
    FeedCardComponent,
    StripHtmlTagsPipe,
    FeeddComponent,
    FluxComponent,
    YoutubListComponent,
    BarComponent,
    LineComponent,
    PieComponent,
    TestapiComponent,
    MergeRequestAPIComponent,
    ListBranchesComponent,
    ListOfCommitComponent
 

   ],

  imports: [
    BrowserModule,
    GrowlModule,
    AngularFirestoreModule,
    ListboxModule,
    TabViewModule,
    ScrollPanelModule,
    PaginatorModule,
    LightboxModule,
    PasswordModule,
    DataTableModule,
    ToolbarModule,
    AutoCompleteModule,
    BrowserModule,
    AccordionModule,
    MultiSelectModule,
    AngularFireModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    TableModule,
    CalendarModule,
    CheckboxModule,
    DataViewModule,
    ChartModule,
    HttpClientModule,
    FormsModule,
    DialogModule,
    SharedModule,
    PanelModule,
    ReactiveFormsModule,
    InputTextModule,
    InputTextareaModule,
    StepsModule,
    ButtonModule,
    HttpModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(environment.firebaseConfig, 'angular-auth-firebase'),
    MenubarModule,
    ProgressBarModule,
    ReactiveFormsModule,
    MessageModule,
    MessagesModule,
    SelectButtonModule,
    FileUploadModule,
    DialogModule,
    ConfirmDialogModule,
    Ng2GoogleChartsModule,
    DropdownModule,
   

  ],


  providers: [
    AngularFireAuth,
    AngularFireDatabase,
    MessageService,
    UserService,
    ConfirmationService,
     FeedService,
     FeedssService,
     TestapiService,
     MergeRequestAPIService



   
  ],
  
  

  bootstrap: [AppComponent]
})
export class AppModule { }
