import { Component, OnInit } from '@angular/core';
import { FormGroup,FormArray,FormBuilder,Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { Link } from '../../slide1/link';
import { FirebaseListObservable } from 'angularfire2/database-deprecated';
import { MessageService } from 'primeng/components/common/messageservice';
import { Router } from '@angular/router';
import { Button } from 'primeng/button';
import { User } from '../../Models/user';
import { InaamService } from '../../inaam.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {
  display: boolean= true;
  show: boolean= false;
  showDeletedMessage: boolean;

  link = [];
  buttons = [];
  currentUser = {} as User ;
  button = []

  public firebase;
  profileForm: FormGroup
  constructor(public fb: FormBuilder,
   private inaamService : InaamService,
  

  private messageService : MessageService,
  public router : Router,
  private angularFire: AngularFireDatabase,
  private afd: AngularFireDatabase, 
  public userService: UserService) {

      this.profileForm = this.fb.group({
          name: [''],
          url: [''],
    
          aliases: this.fb.array([
          this.fb.control('' )
       
        ])
        
      });
    }
    
  ngOnInit() { 
      // this.savedb()
  
      //  this.fetchData()
      this.getdata(),
      this.savel()
        
          this.currentUser = JSON.parse(localStorage.getItem("currentUser")) as User ;
 
  }
       save(name , url){
    this.display = true;
    this.buttons.push({
      'name' : name,
      'url' : url
    })

    

  }

// deletel(link) {
  
 //this.display = true;
              //  this.afd.list("/Link/" + link).remove().then(() => {
                
               // this.getdata();
              
               //})
          
             // }

        
        
           // }
         
        

  

  addlink() {
   
    this.aliases.push(this.fb.control(''));
  }
  get aliases() {
    return this.profileForm.get('aliases') as FormArray;
  }

 // savebd(){
   /// this.afd.list("/Link/")
    ///.snapshotChanges().subscribe(data => {
     /// this.buttons = [];
     /// data.forEach(element => {
        ///var y = element.payload.toJSON();
       /// y["$key"] = element.key;
       /// this.buttons.push(y as Link);
   
        ///console.log(this.buttons);
     /// });
   
  

///});

//}

     showDialog() {
        this.display = !this.display;
    }


   // fetchData() {


      //this.afd.list("/Link/")
        //.snapshotChanges().subscribe(data => {
        // this.buttons = [];
        // data.forEach(element => {
           //var y = element.payload.toJSON();
           // y["$key"] = element.key;
          // this.buttons.push(y as Link);
       
       // });
        //this.link = this.buttons,
        ///console.log(this.link)
        //console.log(this.buttons); 
         
     // });
  //  }
     
         
        
 // });
     
        
        
   // }
  
   deleteform(name , url){
   this.profileForm.reset(name, url);
     console.log("hello")

 }
    
    
    //savedb(){
    // return  this.afd.list("/Link/").push(this.buttons),
     //console.log("hello")
    // }
    savel(){
      console.log('save')
      this.display = true;
      return  this.afd.list("/Link/").push(this.buttons),
      this.getdata(),
     console.log("hello")

    }
    delete(){
      console.log('delete')
    }
    getdata(){
      console.log('get')
      this.display = true;

      this.afd.list("/Link/")
        .snapshotChanges().subscribe(data => {
         this.link = [];
         data.forEach(element => {
           var y = element.payload.toJSON();
            y["$key"] = element.key;
           this.link.push(y as Link);
       
        });
     // this.link = this.buttons,
      console.log(this.link)
      
      
         
     });
  // }
     
         
        
 // });
    }
    deletef($key){
      if (confirm('Are you sure to delete this record ?')) {
        this.inaamService.deletelink($key);
        this.showDeletedMessage = true;
        this.profileForm.reset($key);
        setTimeout(() => this.showDeletedMessage = false, 3000);
 
      }
    }
    }

  


