import { Hotel } from './hotel';
import { Flight } from './flight';
import { User } from './user';

export class Mission {

    $key : string ;
    object : string ; 
    destination :string ;
    city :string ;
    websites :string ;
    startDate :any ;
    endDate :any ;
    paperType? : string ;
    paperAttachemet?: string ;
    missionFlight : Flight; 
    missionHotel : any ; 
    missionStatus: string; 
    user: User ;
    missionTotalPrice : string; 
}
