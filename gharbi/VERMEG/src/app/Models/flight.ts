export class Flight {

    airline: string;
    departure_date: any;
    destination: string;
    price: string;
    return_date: string;

}
