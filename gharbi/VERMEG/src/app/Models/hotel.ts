export class Hotel {

    location_longitude :string ; 
    location_latitude : string ; 
    property_name :string ; 
    address_city :string  ; 
    address_region :string; 
    total_price_amount : string; 
    total_price_currency :string ;
    min_daily_rate_amount :string ;

}
