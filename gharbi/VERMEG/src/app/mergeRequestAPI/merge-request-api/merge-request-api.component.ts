import { Component, OnInit } from '@angular/core';
import { MergeRequestAPIService } from 'src/app/services/merge-request-api.service';

@Component({
  selector: 'app-merge-request-api',
  templateUrl: './merge-request-api.component.html',
  styleUrls: ['./merge-request-api.component.css']
})
export class MergeRequestAPIComponent implements OnInit {
  mergeRequestList: any[] = [];
  public cols: any[];
  columnOptions: any[];

  constructor(private mergeRequestAPI: MergeRequestAPIService) { }
  ngOnInit(): void {
    this.cols = [
      { field: 'id', header: 'id', index: 1 },
      { field: 'username', header: 'username', index: 2 },
      { field: 'brand', header: 'Brand', index: 3 },
      { field: 'color', header: 'Color', index: 4 }
    ];
    this.columnOptions = [];

    for (let i = 0; i < this.cols.length; i++) {
      this.columnOptions.push({label: this.cols[i].header, value: this.cols[i]});
    }
    this.mergeRequestAPI.getMergeRequestList().subscribe(res => {
      this.mergeRequestList = res;
    }, err => {
        return console.log(err);
      });
  }

}
