import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeRequestAPIComponent } from './merge-request-api.component';

describe('MergeRequestAPIComponent', () => {
  let component: MergeRequestAPIComponent;
  let fixture: ComponentFixture<MergeRequestAPIComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeRequestAPIComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeRequestAPIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
