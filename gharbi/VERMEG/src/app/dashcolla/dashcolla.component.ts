import { Component, OnInit } from '@angular/core';
import { User } from './../Models/user';
import { AngularFireDatabase } from 'angularfire2/database';
import {CustomUser } from '../dash/dashadmin/CustomUser'
import { Mission } from '../Models/mission';
import { MessageService } from 'primeng/components/common/messageservice';
@Component({
  selector: 'app-dashcolla',
  templateUrl: './dashcolla.component.html',
  styleUrls: ['./dashcolla.component.css']
})
export class DashcollaComponent implements OnInit {
  myUid: any;
  missionList: Mission[] = [];
  currentUser = {} as User;
  inaam = [];
  userList: User[] = [];
  users : CustomUser[] = []
  data: any;


 

  display: boolean = false;
  userToShow: Mission;
 
  
  constructor(   private afd: AngularFireDatabase,
   private messageService : MessageService) { }
  createChart(labels,datas) {
    let chart = {
        "labels" : labels,
        "datasets" : [
            {
                "label" : "# of users",
                "data" : datas,
                backgroundColor: [
                    this.getRandomColorHex(),
                    this.getRandomColorHex(),
                    this.getRandomColorHex(),
                    this.getRandomColorHex(),
                    this.getRandomColorHex()
                  ],
                hoverBackgroundColor:[
                  
                    this.getRandomColorHex(),
                    this.getRandomColorHex(),
                    this.getRandomColorHex(),
                    this.getRandomColorHex(),
                    this.getRandomColorHex()
                      
                ]
            }
        ]
    }
    return chart
  }

  fetchData() {
  
    this.currentUser = JSON.parse(localStorage.getItem("currentUser")) as User;
    this.afd.list("/missions/", ref => ref.orderByChild("user/uid").equalTo(this.currentUser.uid))
      .snapshotChanges().subscribe(data => {
        this.missionList = [];
        data.forEach(element => {
          var y = element.payload.toJSON();
          y["$key"] = element.key;
          this.missionList.push(y as Mission);
        });
       
     
      
        let ourUser : CustomUser = {
            firstName : this.missionList[0].user.firstname,
            lastName : this.missionList[0].user.lastname,
            email : this.missionList[0].user.email,
            nbOfMissions : 1 
        }
        this.users.push(ourUser)
        this.missionList.forEach((mission,index) => {

             if(index>0){
                let isExist : boolean = false
                let indexIsExist : number
                 this.users.forEach((customUser,index2) => {
                   
                     if(mission.user.email == customUser.email){
                         isExist=true
                         indexIsExist=index2

                    }  
                }) 
                if(!isExist){
                    let tempUser : CustomUser = {
                        firstName : mission.user.firstname,
                        lastName : mission.user.lastname,
                        email : mission.user.email,
                        nbOfMissions : 1
                     }
                    this.users.push(tempUser)
                 
             
                }
                else{
                    this.users[indexIsExist].nbOfMissions++
                  
                }
            } 
        })

        let labels : string [] = []
        let datas : number [] = []
        this.users.forEach(item => {
          
   
            labels.push(item.firstName)
            datas.push(item.nbOfMissions)
            console.log(item.firstName)
            console.log(item.nbOfMissions)
        })
       
        this.data=this.createChart(labels,datas)
        console.log(this.data)
       

      });
    
      

  }
 getRandomColorHex() {
    var hex = "0123456789ABCDEF",
        color = "#";
    for (var i = 1; i <= 6; i++) {

      color += hex[Math.floor(Math.random() * 16)];
    }
    return color;

  }

  ngOnInit() {
    this.fetchData()
    console.log(this.users)
    
  }




  }
  