import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashcollaComponent } from './dashcolla.component';

describe('DashcollaComponent', () => {
  let component: DashcollaComponent;
  let fixture: ComponentFixture<DashcollaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashcollaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashcollaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
