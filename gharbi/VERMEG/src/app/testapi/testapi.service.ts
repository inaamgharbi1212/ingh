import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TestapiService {
  public  getAllUsersUrl = 'https://gitlab.com/api/v4/projects/6853087';


  constructor(public  http: HttpClient) { }
  getAllUsers() {
    return this.http.get<any>(this.getAllUsersUrl);
  }
}
