import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TestapiService } from './testapi.service';

@Component({
  selector: 'app-testapi',
  templateUrl: './testapi.component.html',
  styleUrls: ['./testapi.component.css']
})
export class TestapiComponent implements OnInit {
peopleList = [];
  constructor(public  testapi: TestapiService) { }

ngOnInit(): void {
  this.testapi.getAllUsers().subscribe(
    result => {
      this.peopleList = result;
    },
    error => {
      console.log(error);
    }
  );
}

}
