import { Component, OnInit } from '@angular/core';
import { FormGroup,FormArray,FormBuilder,Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { Link } from '../slide1/link';
import { FirebaseListObservable } from 'angularfire2/database-deprecated';
import { MessageService } from 'primeng/components/common/messageservice';
import { Router } from '@angular/router';
import { Button } from 'primeng/button';
import { User } from '../Models/user';

@Component({
  selector: 'app-inaamtest',
  templateUrl: './inaamtest.component.html',
  styleUrls: ['./inaamtest.component.css']
})
export class InaamtestComponent implements OnInit {
  profileForm: FormGroup;
  display: boolean= true;

  buttons = [];



  public firebase;

  constructor(public fb : FormBuilder) {
    this.profileForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: [''],
      address: this.fb.group({
        street: [''],
        city: [''],
        state: [''],
        zip: ['']
      }),
      aliases: this.fb.array([
        this.fb.control('')
      ])
    });
   }

  ngOnInit() {
    
  }
  get aliases() {
    return this.profileForm.get('aliases') as FormArray;
  }
  addAlias() {
    this.aliases.push(this.fb.control(''));
  }
  save(name , url){
    this.display = true;
    this.buttons.push({
      'name' : name,
      'url' : url
    })
    console.log(this.buttons)
  }
  showDialog() {
    this.display = !this.display;
}
}
