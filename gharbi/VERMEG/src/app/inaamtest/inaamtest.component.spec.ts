import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InaamtestComponent } from './inaamtest.component';

describe('InaamtestComponent', () => {
  let component: InaamtestComponent;
  let fixture: ComponentFixture<InaamtestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InaamtestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InaamtestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

})
});
