import { TestBed, inject } from '@angular/core/testing';

import { InaamService } from './inaam.service';

describe('InaamService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InaamService]
    });
  });

  it('should be created', inject([InaamService], (service: InaamService) => {
    expect(service).toBeTruthy();
  }));
});
