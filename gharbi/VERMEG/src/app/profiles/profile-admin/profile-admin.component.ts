import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { ConfirmationService } from 'primeng/components/common/confirmationservice';
import { AngularFireAuth } from 'angularfire2/auth';
import { MessageService } from 'primeng/components/common/messageservice';
import { Message } from 'primeng/primeng';
import { Mission } from '../../Models/mission';
import { User } from '../../Models/user';

@Component({
  selector: 'app-profile-admin',
  templateUrl: './profile-admin.component.html',
  styleUrls: ['./profile-admin.component.css']
})
export class ProfileAdminComponent implements OnInit {
  itemArray = []
  userToShow: User;
  itemList: AngularFireList<any>;
  currentUser = {} as User;
  userList: User[] = [];
  showLoading: boolean = true;
  display: boolean = false;

  msgs: Message[] = [];
   constructor(private afd: AngularFireDatabase,
    private confirmationService: ConfirmationService,
    private messageService: MessageService, 
    private auth : AngularFireAuth,
  ) { }
    
  ngOnInit() {
    this.fetchData();
    
  }
   showUser(userDetails: User) {
    this.display = true;
    this.userToShow = userDetails;
  }


    fetchData() {

      this.currentUser = JSON.parse(localStorage.getItem("currentUser")) as User;
      this.showLoading = true;
      this.afd.list("/users")
        .snapshotChanges().subscribe(data => {
          this.userList = [];
          data.forEach(element => {
            var y = element.payload.toJSON();
            y["$key"] = element.key;
            this.userList.push(y as User);
            return this.currentUser
          });
          this.showLoading = false;
          console.log(this.currentUser);
  
        });
 
        
  }
 


}
