import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MergeRequestAPIService {
  apiUrl = '' ;

  constructor(private http: HttpClient) { }
  getMergeRequestList() {
    return this.http.get<any[]>('https://gitlab.com/api/v4/projects/6853087/merge_requests');
  }
  getBranchesList() {
    return this.http.get<any[]>('https://gitlab.com/api/v4/projects/18225253/repository/branches');
  }
  getListOfCommit() {
    return this.http.get<any[]>('https://gitlab.com/api/v4/projects/18225253/repository/commits?private_token=sCDMyWxnb7ce23kFszCV');
  }
}
