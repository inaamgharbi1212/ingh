import { AngularFireAuth } from 'angularfire2/auth';
import { User } from './../Models/user'
import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { Link } from '../slide1/link';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  public isLogged : boolean = false ;

  constructor(private afd: AngularFireDatabase, private auth:AngularFireAuth) { }
  

  registerCollaborater(collaborater : User){
    return this.afd.list("/users/").push(collaborater);
  }

  
  getUserByUID(uid : string){
   return this.afd.list("/users/", ref=>ref.orderByChild("uid").equalTo(uid)) ;
  }

  isLoggedIn(){
   return  this.auth.authState ;
   }
 


   userLogged(value : boolean){
    this.isLogged = value; 
   }
   registerlink(buttons : Link){
    return this.afd.list("/Link/").push(buttons);
  }



}
