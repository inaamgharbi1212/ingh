import { TestBed, inject } from '@angular/core/testing';

import { MergeRequestAPIService } from './merge-request-api.service';

describe('MergeRequestAPIService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MergeRequestAPIService]
    });
  });

  it('should be created', inject([MergeRequestAPIService], (service: MergeRequestAPIService) => {
    expect(service).toBeTruthy();
  }));
});
