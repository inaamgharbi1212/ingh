import { Mission } from './../Models/mission';
import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class MissionService {

  missionNode : AngularFireList<{}>  ;

  constructor(private afd : AngularFireDatabase) {

    this.missionNode = this.afd.list("/missions/");

   }

  getMissionByUID(uid : string ){
   return this.afd.list("/missions/", ref=>ref.orderByChild("user/uid").equalTo(uid)).snapshotChanges() ;
  }
  

  insertMission(mission: Mission){
    return this.missionNode.push(mission);
  }


  convertDateToTimeStamp(plainDate : string){
    let tmpDate = new Date(plainDate).valueOf();
    return tmpDate; 
  }


}
