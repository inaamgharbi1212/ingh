import { SelectItem } from 'primeng/components/common/selectitem';
import { UserService } from './../../services/user.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database-deprecated';
import { MessageService } from 'primeng/components/common/messageservice';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Message } from 'primeng/components/common/message';
import { Component, OnInit } from '@angular/core';
import { User } from '../../Models/user';
import { DropdownModule } from 'primeng/dropdown';

@Component({
  selector: 'app-add-user-admin',
  templateUrl: './add-user-admin.component.html',
  styleUrls: ['./add-user-admin.component.css']
})
export class AddUserAdminComponent implements OnInit {

  showProgressBar: boolean = false;
  email:string = '';
  uid:any;
  user = {} as User;

  userTypes: SelectItem[];

  msgs: Message[] = [];

  registerForm = new FormGroup({

    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [Validators.minLength(6), Validators.required]),
    userType: new FormControl([Validators.required])

  });

  constructor(private messageService: MessageService,
    public db: AngularFireDatabase,
    private auth: AngularFireAuth,
    private userService: UserService) { }

  ngOnInit() {
    this.userTypes = [
      { label: 'Collaborator', value: "collaborator" },
      { label: 'Validator', value: "validator" },
      { label: 'Admin', value: "admin" }

    ];
  }



  submitRegisterForm() {
    console.log(this.registerForm.valid);
    if (!this.registerForm.valid || this.user.userType == undefined) {
      this.messageService.add({ severity: 'error', summary: 'Form Validation', detail: 'Form is Invalid !' });
    } else {
      this.registerForm.disable();

      this.showProgressBar = true;

      console.log(this.user);

      this.auth.auth.createUserWithEmailAndPassword(this.user.email, this.user.password).then(userData => {
        this.user.uid = userData.user.uid;

        this.userService.registerCollaborater(this.user).then(() => {
          this.showProgressBar = false;
          this.messageService.add({ severity: 'success', summary: 'Register info', detail: 'User has been added successfully ! ' });

          this.registerForm.enable();
         
        });

    let user =  localStorage.getItem('email')
    this.email = user
    console.log( user)
    console.log( '-----------------------')

    this.uid =  localStorage.getItem('uid')
    console.log( 'uid: '+ this.uid)

      }).catch(err => {
        console.log(err);
        this.showProgressBar = false;
        this.messageService.add({ severity: 'error', summary: 'Register error', detail: err.message });


      });


    }




  }


}
