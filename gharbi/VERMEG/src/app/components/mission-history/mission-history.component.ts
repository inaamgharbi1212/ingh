import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';
import { Mission } from './../../Models/mission';
import { User } from './../../Models/user';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProgressBarModule } from 'primeng/progressbar';
import { DialogModule } from 'primeng/dialog';
import { GrowlModule } from 'primeng/growl';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService, SelectItem } from 'primeng/api';
import { Message } from 'primeng/components/common/api';
import { PanelModule } from 'primeng/panel';

@Component({
  selector: 'app-mission-history',
  templateUrl: './mission-history.component.html',
  styleUrls: ['./mission-history.component.css']
})
export class MissionHistoryComponent implements OnInit {

  missionList: Mission[] = [];
  currentUser = {} as User;
  showLoading: boolean = true;
  display: boolean = false;
  missionToShow: Mission;
  missionToEdit: Mission;
  msgs: Message[] = [];
  displayEdit: boolean = false;



  missionDescreptionForm = new FormGroup({

    missionObject: new FormControl('', [Validators.required]),
    missionDestionation: new FormControl('', [Validators.required]),
    missionCity: new FormControl('', [Validators.required]),
    userWebsite: new FormControl('', [Validators.required]),
    missionStartDate: new FormControl('', [Validators.required]),
    missionEndDate: new FormControl('', [Validators.required]),
  });

  paperType: SelectItem[];





  constructor(private router: Router,
    private afd: AngularFireDatabase,
    private messageService: MessageService,
    private confirmationService: ConfirmationService) { }

  // , ref=>ref.orderByChild("user/uid").equalTo(this.currentUser.uid)
  ngOnInit() {

    console.log("History ");
    this.fetchData();

    this.paperType = [
      { label: 'No', value: "no", icon: "fas fa-times-circle", },
      { label: 'VISA', value: "visa", icon: "fab fa-fw fa-cc-visa" },
      { label: 'Passport', value: "passport", icon: "fas fa-fw fa-passport" },
    ];


  }


  fetchData() {

    this.currentUser = JSON.parse(localStorage.getItem("currentUser")) as User;
    this.showLoading = true;
    this.afd.list("/missions/", ref => ref.orderByChild("user/uid").equalTo(this.currentUser.uid))
      .snapshotChanges().subscribe(data => {
        this.missionList = [];
        data.forEach(element => {
          var y = element.payload.toJSON();
          y["$key"] = element.key;
          this.missionList.push(y as Mission);
        });
        this.showLoading = false;
        console.log(this.missionList);

      });
  }

  editMission($event) {
    console.log($event);
    this.displayEdit = true;
    this.missionToEdit = $event;
  }

  deleteMission(mission) {



    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete "<b>' + mission.object + '</b>"',
      accept: () => {
        this.afd.list("/missions/" + mission.$key).remove().then(() => {
          this.messageService.add({ severity: 'success', summary: 'Mission deleted', detail: 'The mission has been deleted' });
          this.fetchData();
        });
      }
    });



  }

  showMission(missionDetails: Mission) {
    this.display = true;
    this.missionToShow = missionDetails;
  }


  checkDateValidity(): boolean {
    let startDateTimestamp = new Date(this.missionToEdit.startDate).valueOf();
    let endDateTimestamp = new Date(this.missionToEdit.endDate).valueOf();
    if (startDateTimestamp >= endDateTimestamp) return false;
    else return true;
  }

  submitEditMission() {

    if (!this.missionDescreptionForm.valid) {
      this.messageService.add({ severity: 'error', summary: 'Invalid Form', detail: 'Verify all the fields ' });
    } else if (this.checkDateValidity() == false) {
      this.messageService.add({ severity: 'error', summary: 'Invalid Form', detail: 'Verify Start Date and End Date ' });
    } else {

      console.log("updating");
      console.log(this.missionToEdit);
      let key: string = this.missionToEdit.$key;
      console.log(key);

      delete this.missionToEdit.$key ;

      this.afd.list("/missions/").update(key, this.missionToEdit).then(data => {

        this.messageService.add({ severity: 'success', summary: 'Mission Update', detail: 'The mission has been updated !' });
        this.fetchData();
        this.displayEdit = false ;
      }).catch((err: Error) => {

        this.messageService.add({ severity: 'error', summary: 'Update Error', detail: err.message });

      });
    }


  }



}
