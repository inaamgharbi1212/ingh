import { Component, OnInit, Input } from '@angular/core';
import { MergeRequestAPIService } from 'src/app/services/merge-request-api.service';
import { Mission } from 'src/app/Models/mission';
import { Branche } from 'src/app/Models/branche';

@Component({
  selector: 'app-list-branches',
  templateUrl: './list-branches.component.html',
  styleUrls: ['./list-branches.component.css']
})
export class ListBranchesComponent implements OnInit {
  lsitOfBranches: any[] = [];
  // tslint:disable-next-line: no-input-rename
  selectedCity1: Branche;

  constructor(private mergeRequestAPI: MergeRequestAPIService) { }

  ngOnInit() {

    this.mergeRequestAPI.getBranchesList().subscribe(res => {
      this.lsitOfBranches = res;
    }, err => {
        return console.log('this.lsitOfBranches');
      });

  }


}
