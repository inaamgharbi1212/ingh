import { AngularFireAuth } from 'angularfire2/auth';
import { Message } from 'primeng/components/common/api';
import { Mission } from './../../Models/mission';
import { MessageService } from 'primeng/components/common/messageservice';
import { ConfirmationService } from 'primeng/api';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit } from '@angular/core';
import { DialogModule } from 'primeng/dialog';

@Component({
  selector: 'app-users-admin',
  templateUrl: './users-admin.component.html',
  styleUrls: ['./users-admin.component.css']
})
export class UsersAdminComponent implements OnInit {


  userList: Mission[] = [];
  showLoading: boolean = true;
  display: boolean = false;
  userToShow: Mission;
  msgs: Message[] = [];


  constructor(private afd: AngularFireDatabase,
    private confirmationService: ConfirmationService,
    private messageService: MessageService, 
    private auth : AngularFireAuth) { }

  ngOnInit() {
    this.fetchData();
  }


  fetchData() {

    this.showLoading = true;
    this.afd.list("/users/")
      .snapshotChanges().subscribe(data => {
        this.userList = [];
        data.forEach(element => {
          var y = element.payload.toJSON();
          y["$key"] = element.key;
          this.userList.push(y as Mission);
        });
        this.showLoading = false;
        console.log(this.userList);

      });
  }

  deleteUser(user){
    console.log(user);
     this.confirmationService.confirm({
      message: 'Are you sure that you want to delete "<b></b>"',
      accept: () => {
        this.afd.list("/users/" + user.$key).remove().then(() => {
          this.messageService.add({ severity: 'success', summary: 'Mission deleted', detail: 'The mission has been deleted' });
          this.fetchData();
        
        });
      }
    });



  }



 
}
