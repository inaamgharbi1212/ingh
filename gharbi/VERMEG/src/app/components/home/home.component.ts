import { UserService } from './../../services/user.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { Component, OnInit } from '@angular/core';
import { User } from '../../Models/user';
import {ButtonModule} from 'primeng/button';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  currentUser = {} as User ;
  constructor(private auth : AngularFireAuth, public userService : UserService) { }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser")) as User ;
    
  }

  logOut(){
    console.log("---logout---");
    localStorage.removeItem("currentUser");
    localStorage.clear();
    this.auth.auth.signOut().then(data=>{
      this.userService.isLogged = false ;
      console.log(data);
    }) ;

  }

}
