import { UserService } from './../../services/user.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { ProgressBarModule } from 'primeng/progressbar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { GrowlModule } from 'primeng/growl';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';
import { User } from '../../Models/user';


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  email: string = "";
  password: string = "";
  
  showProgressBar : boolean = false ;

  msgs: Message[] = [];

  signInForm = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [Validators.minLength(6), Validators.required])
  });

  constructor(private auth: AngularFireAuth,
    private userService: UserService, 
    private messageService: MessageService,
    private router: Router, 
 
    ) { }


  ngOnInit() {
  
  }


  submitSignInForm(){
    
    console.log(this.signInForm.valid);
    if(!this.signInForm.valid){
      this.messageService.add({ severity: 'error', summary: 'Login error', detail: 'Form is Invalid ! ' });
    }else{
      this.signInForm.disable();
      this.showProgressBar = true ; 
      this.auth.auth.signInWithEmailAndPassword(this.email, this.password).then(userData=>{
        
      console.log(userData);
      
      this.userService.getUserByUID(userData.user.uid).valueChanges()
      .subscribe(data=>{

        let currentUser = data[0] as User ; 
        delete currentUser.password ; 

        localStorage.setItem("currentUser", JSON.stringify(currentUser));

        console.log(currentUser);
        this.showProgressBar = false ;
        
        console.log("navigate");
        this.userService.isLogged = true ;
        

        this.router.navigate(['/home']);

        if(currentUser.userType=="collaborator"){
          this.router.navigate(['history']);
        }else if (currentUser.userType=="admin"){
          this.router.navigate(['usersAdmin']);
        }else{
          this.router.navigate(['validateMissions']);
        }
        
        

      });

      }).catch(err=>{
        this.signInForm.enable();
        this.showProgressBar = false ;
        this.messageService.add({ severity: 'error', summary: 'Login error', detail:err.message });
      });


    }
    

  }


}

