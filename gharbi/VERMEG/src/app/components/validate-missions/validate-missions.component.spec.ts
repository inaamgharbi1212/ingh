import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidateMissionsComponent } from './validate-missions.component';

describe('ValidateMissionsComponent', () => {
  let component: ValidateMissionsComponent;
  let fixture: ComponentFixture<ValidateMissionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidateMissionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidateMissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
