import { MessageService } from 'primeng/components/common/messageservice';
import { AngularFireDatabase } from 'angularfire2/database';
import { Message } from 'primeng/components/common/api';
import { Mission } from './../../Models/mission';
import { User } from './../../Models/user';
import { Component, OnInit } from '@angular/core';
import { ProgressBarModule } from 'primeng/progressbar';
import { DialogModule } from 'primeng/dialog';
import { GrowlModule } from 'primeng/growl';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { stat } from 'fs';

@Component({
  selector: 'app-validate-missions',
  templateUrl: './validate-missions.component.html',
  styleUrls: ['./validate-missions.component.css']
})
export class ValidateMissionsComponent implements OnInit {


  missionList: Mission[] = [];
  showLoading: boolean = true;
  display: boolean = false;
  missionToShow: Mission;
  msgs: Message[] = [];





  constructor(private afd: AngularFireDatabase,
    private confirmationService: ConfirmationService,
    private messageService: MessageService) { }

  ngOnInit() {
    this.fetchData();
  }


  fetchData() {

    this.showLoading = true;
    this.afd.list("/missions/")
      .snapshotChanges().subscribe(data => {
        this.missionList = [];
        data.forEach(element => {
          var y = element.payload.toJSON();
          y["$key"] = element.key;
          this.missionList.push(y as Mission);
        });
        this.showLoading = false;
        console.log(this.missionList);

      });
  }

  showMission(missionDetails: Mission) {
    this.display = true;
    this.missionToShow = missionDetails;
  }


  deleteMission(mission) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to delete "<b>' + mission.object + '</b>"',
      accept: () => {
        this.afd.list("/missions/" + mission.$key).remove().then(() => {
          this.messageService.add({ severity: 'success', summary: 'Mission deleted', detail: 'The mission has been deleted' });
          this.fetchData();
        });
      }
    });
  }

  acceptRejectMission(mission : Mission, status: string){
    console.log("accept Reject mission");
    if(status=='r'){

      this.confirmationService.confirm({
        message: 'Are you sure that you want to reject the mission "<b>' + mission.object + '</b>"',
        accept: () => {
          this.afd.list("/missions/").update(mission.$key, {missionStatus:"rejected"}).then(() => {
            this.messageService.add({ severity: 'success', summary: 'Mission Rejection', detail: 'The mission has been rejected' });
            this.fetchData();
          });
        }
      });



    }else if(status=='a'){
      this.confirmationService.confirm({
        message: 'Are you sure that you want to accept the mission "<b>' + mission.object + '</b>"',
        accept: () => {
          this.afd.list("/missions/").update(mission.$key, {missionStatus:"accepted"}).then(() => {
            this.messageService.add({ severity: 'success', summary: 'Mission Accept', detail: 'The mission has been accepted' });
            this.fetchData();
          });
        }
      });
    }

    
  }


}
