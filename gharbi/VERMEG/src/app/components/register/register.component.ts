import { UserService } from './../../services/user.service';
import { User } from './../../Models/user';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database'
import { ProgressBarModule } from 'primeng/progressbar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { GrowlModule } from 'primeng/growl';
import { Message } from 'primeng/components/common/api';
import { MessageService } from 'primeng/components/common/messageservice';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  myImage:string="http://www.cjnotebook.com/wp-content/uploads/2017/10/airline.jpg";
  Img:string="http://acainsuranceday.lu/wp-content/uploads/vermeg_logo_small.png";
  icon:string="http://www.icons101.com/icon_ico/id_79389/user.ico";
  showProgressBar: boolean = false;

  user = {} as User;

  msgs: Message[] = [];

  registerForm = new FormGroup({

    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [Validators.minLength(6), Validators.required]),

  });

  constructor(private messageService: MessageService,
    public db: AngularFireDatabase,
    private auth: AngularFireAuth,
    private router: Router,
    private userService: UserService
  ) {

  }

  ngOnInit() {
    this.user.uid = "";
    this.user.userType = "collaborator";
  }


  submitRegisterForm() {
    console.log(this.registerForm.valid);
    if (!this.registerForm.valid) {
      this.messageService.add({ severity: 'error', summary: 'Form Validation', detail: 'Form is Invalid !' });
    } else {
      this.registerForm.disable();
      this.showProgressBar = true;

      console.log(this.user);

      this.auth.auth.createUserWithEmailAndPassword(this.user.email, this.user.password).then(userData => {
        this.user.uid = userData.user.uid;

        this.userService.registerCollaborater(this.user).then(() => {
          this.showProgressBar = false;
          this.messageService.add({ severity: 'success', summary: 'Register info', detail: 'You have been successfully registered ! ' });
          this.registerForm.enable();
          setTimeout(() => {
            this.router.navigate(['/']);
          }, 2500);

        });

      }).catch(err => {
        console.log(err);
        this.registerForm.enable();
        this.showProgressBar = false;
        this.messageService.add({ severity: 'error', summary: 'Register error', detail: err.message });


      });


    }




  }



}


