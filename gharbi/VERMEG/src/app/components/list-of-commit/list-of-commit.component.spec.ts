import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListOfCommitComponent } from './list-of-commit.component';

describe('ListOfCommitComponent', () => {
  let component: ListOfCommitComponent;
  let fixture: ComponentFixture<ListOfCommitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListOfCommitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListOfCommitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
