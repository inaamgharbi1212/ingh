import{Feede} from '../Model2/feed-e'
import{Feedi} from '../Model2/feed-i'


export interface Feed {
  status: string,
  feed: Feedi,
  items: Array<Feede>
}