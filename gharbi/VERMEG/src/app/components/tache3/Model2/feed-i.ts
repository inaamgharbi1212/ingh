export interface Feedi {
    title: string,
    link: string,
    author: string,
    description: string,
    image: string
  }
  