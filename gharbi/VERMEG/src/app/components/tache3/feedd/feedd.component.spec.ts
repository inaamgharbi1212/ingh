import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeeddComponent } from './feedd.component';

describe('FeeddComponent', () => {
  let component: FeeddComponent;
  let fixture: ComponentFixture<FeeddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeeddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeeddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
