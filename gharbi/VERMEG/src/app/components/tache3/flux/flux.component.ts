import { Component, OnInit } from '@angular/core';

  import { FeedssService } from '../../tache3/feedss.service'

@Component({
  selector: 'app-flux',
  templateUrl: './flux.component.html',
  styleUrls: ['./flux.component.css']
})
export class FluxComponent implements OnInit {
 
  private feedUrl: string = ' http://www.futura-sciences.com/rss/sante/actualites.xml?channel_id=UCHnyfMqiRRG1u-2MsSQLbXA';
 private img : string =''
  private feeds:any

  constructor (
    private feedService: FeedssService 
  ) {}

  ngOnInit() {
    this.refreshFeed();
    
  }

  private refreshFeed() {
    this.feedService.getFeedContent(this.feedUrl)
        .subscribe(
            feed => this.feeds = feed.items ,
            error => console.log(error)) ;
  }

}