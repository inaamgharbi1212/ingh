import { TestBed, inject } from '@angular/core/testing';

import { FeedssService } from './feedss.service';

describe('FeedssService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FeedssService]
    });
  });

  it('should be created', inject([FeedssService], (service: FeedssService) => {
    expect(service).toBeTruthy();
  }));
});
